from netCDF4 import Dataset


class NetCDFAnalyzer:

    def __init__(self):
        self.__dataset = None

    def open(self, file_path):
        self.__dataset = Dataset(file_path)
        return self.__dataset is not None

    def is_open(self):
        return self.__dataset is not None

    def __open(self, file_path):
        return Dataset(file_path)

    def get_file_format(self):
        return self.__dataset.groups['PRODUCT'].file_format

    def get_dimensions_keys(self):
        return self.__dataset.groups['PRODUCT'].dimensions.keys()

    def show_all_dimensions_info(self):
        print('Dimensions info: ')
        keys = self.get_dimensions_keys()
        for key in keys:
            info = self.__dataset.groups['PRODUCT'].dimensions[key]
            print(key + ":\n" + str(info))

    def show_dimensions_names(self):
        print(self.get_dimensions_keys())

    def get_variables_keys(self):
        return self.__dataset.groups['PRODUCT'].variables.keys()

    def show_all_variables_info(self):
        print('Variables info: ')
        variables = self.get_variables_keys()
        for variable in variables:
            info = self.__dataset.groups['PRODUCT'].variables[variable]
            print(variable + ":\n" + str(info))

    def show_variables_names(self):
        print(self.get_variables_keys())

    def show_variables_names_short(self):
        print("List of attributes:")
        variables = self.get_variables_keys()
        for variable in variables:
            print(variable)

    def get_conventions_attribute(self):
        return self.__dataset.Conventions

    def show_all_global_attributes(self):
        print('Global attributes:')
        for attr in self.__dataset.ncattrs():
            print(attr, "=", getattr(self.__dataset, attr))

    def close(self):
        self.__dataset.close()

    #co to mialo robic?
    #def show_data(self):
    #    print(self.__dataset.dimensions['npco'])

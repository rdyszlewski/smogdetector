import csv
from regressor.models import Station


def read_file(path:str)->dict:

    stations_dict = {}
    with open(path, 'r', encoding='utf-8') as file:
        reader = csv.reader(file, delimiter=';')
        line_counter = 0

        cities = []
        stations = []
        elements = []

        for row in reader:
            columns = len(row)
            if line_counter == 0: # reading headers

                for column in range(1, columns - 1):
                    values = row[column].split('-')
                    city = values[0].strip()
                    station_name = values[1].strip().rstrip()
                    element = values[2].strip().rstrip()

                    if station_name not in stations_dict:
                        stations_dict[station_name] = Station(station_name)

                    cities.append(city)
                    stations.append(station_name)
                    elements.append(element)
            elif line_counter >= 2:
                time = row[0]
                columns = columns - 1
                for column in range(1, columns - 1):

                    text = row[column].replace('\t','').strip().rstrip()
                    if text:
                        value = float(text)
                    else:
                        value = -1

                    station_name = stations[column-1]
                    element = elements[column-1]


                    stations_dict[station_name].measurements.add(element, time, value)
            line_counter += 1
        print(line_counter)
    return stations_dict
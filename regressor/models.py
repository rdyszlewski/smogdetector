
class Station:

    def __init__(self, station_name):
        self.station = station_name
        self.measurements = Measurements()


class Measurements:

    def __init__(self):

        self.__times = []
        self.__values = {}
        self.__time2index = {}

        self.PM10 = 'pył zawieszony PM10'
        self.PM2_5 = 'pył zawieszony PM2.5'
        self.CO = 'tlenek węgla'
        self.NO = 'tlenek azotu'
        self.NO_2 = 'dwutlenek azotu'

        elements = [self.PM10, self.PM2_5, self.CO, self.NO, self.NO_2]
        for element in elements:
            self.__values[element] = []

    def add(self, element, time, value):
        if time not in self.__time2index:
            self.__times.append(time)
            self.__time2index[time] = len(self.__times) -1

        if element in self.__values:
            index = self.__time2index[time]
            self.__values[element].insert(index, value)

    def get_value(self, element, time):
        if element in self.__values and time in self.__time2index:
            index = self.__time2index[time]
            return self.__values[element][index]



